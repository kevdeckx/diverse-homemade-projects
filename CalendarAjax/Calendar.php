<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ajax Calendar</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<!--DISCLAIMER -->
<!--This was made before I learned about OOP, design patterns, and many other best practices-->
<div id="background"></div>
<div id="modalbox">
    <div id="modalcontent">
        <div id="close" onclick="CloseModal()">X</div>
        <form method="post" action="" onsubmit="event.preventDefault(); submitForm('php/post.php');">
            <label id="lbldate"></label>
            <br>
            <br>
            <input type="hidden" name="hdnday" id="hdnday">
            <input type="hidden" name="hdnmonth" id="hdnmonth">
            <input type="hidden" name="hdnyear" id="hdnyear">
            <textarea name="txtevent" id="txtevent" cols="30" rows="10"></textarea>
            <br>
            <input type="time" name="time" id="time" value="12:00">
            <br>
            <input type="submit" value="Verzenden"></form>
        <br>
        <br>
        <p id="records"></p>
    </div>
</div>
<div id="wrapper">
    <header>
        <h1>Mijn kalender :&#41;</h1></header>
    <table>
        <caption id="currentmonth"></caption>
        <thead>
        <tr>
            <th>Maandag</th>
            <th>Dinsdag</th>
            <th>Woensdag</th>
            <th>Donderdag</th>
            <th>Vrijdag</th>
            <th>Zaterdag</th>
            <th>Zondag</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
        </tr>
        <tr>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
        </tr>
        <tr>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
        </tr>
        <tr>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
        </tr>
        <tr>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
        </tr>
        <tr>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
            <td class="days"></td>
        </tr>
        </tbody>
    </table>
    <nav>
        <ul>
            <li id="previous" onclick="PreviousMonth()">&lt;</li>
            <li id="next" onclick="NextMonth()">&gt;</li>
        </ul>
    </nav>
</div>
<footer>&copy; Kevin Deckx</footer>
<script>

    var months = ["Januari", "Februari", "Maart", "April", "Mei", "Juni", "July", "Augustus", "September", "October", "November", "December"]
        , today = new Date()
        , m = today.getMonth() + 1
        , y = today.getFullYear();
    document.querySelector("footer").innerHTML += today.getFullYear();

    function FirstOfMonth(month, year) {
        var firstOfMonth = new Date(year, month - 1, 1).getDay();
        return firstOfMonth;
    }

    Calendar(m, y, FirstOfMonth(m, y));

    function NextMonth() {
        if (m == 12) {
            m = 0;
            y++;
        }
        m++
        Calendar(m, y, FirstOfMonth(m, y));
    }

    function PreviousMonth() {
        if (m == 1) {
            m = 13;
            y -= 1;
        }
        m--;
        Calendar(m, y, FirstOfMonth(m, y));
    }

    function Calendar(month, year, n) {
        var resetdays, days;
        resetdays = document.querySelectorAll("td");
        //because american week starts with sunday
        n -= 1;
        if (n == -1) {
            n = 6;
        }
        for (var i = 0; i < resetdays.length; i++) {
            resetdays[i].className = "";
            resetdays[i].classList.add("days");
            resetdays[i].onclick = function () {
                //reset function
                return false;
            }
        }
        for (var i = 0; i < n + 1; i++) {
            //what day is the first of the month? -> put in position
            if (i < n) {
                resetdays[i].className = "dummy";
                resetdays[i].innerHTML = "";
            }
            //hide leading zero
            if (i == n) {
                resetdays[i].classList.add("hide");
            }
        }
        days = document.querySelectorAll(".days")
        for (var i = 0; i < days.length; i++) {
            days[i].classList.remove("event");
            days[i].classList.remove("today");
            days[i].innerHTML = i;
            HighlightEvents(i, month, year, days[i]);
            if (days[i].innerHTML > daysInMonth(month, year)) {
                days[i].innerHTML = "";
                days[i].className = "dummy";
            }
            else {
                //display records in modal, put parameters in hidden fields
                days[i].onclick = (function (day, month, year) {
                    return function () {
                        var http = new XMLHttpRequest()
                            , params = "day=" + day + "&month=" + month + "&year=" + year;
                        document.getElementById("modalbox").style.display = "block";
                        document.getElementById("lbldate").innerHTML = "Set event for " + day + "/" + month + "/" + year;
                        http = new XMLHttpRequest;
                        http.open("POST", "php/select.php", true);
                        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        http.send(params);
                        http.onload = function () {
                            document.getElementById("records").innerHTML = http.responseText;
                        }
                        modalbox.style.display = "block";
                        document.getElementById("hdnday").value = day;
                        document.getElementById("hdnmonth").value = month;
                        document.getElementById("hdnyear").value = year;
                    }
                })(i, month, year);
            }
        }
        //display the month name
        document.getElementById("currentmonth").innerHTML = months[m - 1] + " " + y;
    }

    //how many days in a month?
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }

    //query all days and highlight them if record  found
    function HighlightEvents(day, month, year, td) {
        var http = new XMLHttpRequest()
            , params = "day=" + day + "&month=" + month + "&year=" + year;
        http.open("POST", "php/select.php", true);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(params);
        http.onload = function () {
            if (http.responseText != "") {
                td.classList.add("event");
            }
            if (day == today.getDate() && month == today.getMonth() + 1 && year == today.getFullYear()) {
                td.classList.remove("event");
                td.classList.add("today");
            }
        }
    }

    //process form data with ajax
    function submitForm(target, index) {
        var http = new XMLHttpRequest()
            ,
            params = "day=" + document.getElementById("hdnday").value + "&month=" + document.getElementById("hdnmonth").value + "&year=" + document.getElementById("hdnyear").value + "&event=" + document.getElementById("txtevent").value + "&time=" + document.getElementById("time").value + "&index=" + index;

        http.open("POST", target, true);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(params);
        http.onload = function () {
            document.getElementById("records").innerHTML = http.responseText;
        }
        document.getElementById("txtevent").value = "";
        document.getElementById("time").value = "12:00";
    }

    //close modalbox and reset form values
    function CloseModal() {
        document.getElementById("modalbox").style.display = "none";
        document.getElementById("txtevent").value = "";
        document.getElementById("time").value = "12:00";
        document.getElementById("records").innerHTML = "";
        Calendar(m, y, FirstOfMonth(m, y));
    }


</script>
</body>

</html>