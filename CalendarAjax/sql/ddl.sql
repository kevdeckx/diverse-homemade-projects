DROP DATABASE IF EXISTS db_calendar;
CREATE DATABASE db_calendar;

USE db_calendar;

DROP TABLE IF EXISTS t_calendar;
CREATE TABLE t_calendar(
pk_calendar INT AUTO_INCREMENT PRIMARY KEY,
d_date BIGINT,
d_time TIME,
d_event TEXT
)